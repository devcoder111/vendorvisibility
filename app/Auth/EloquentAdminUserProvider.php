<?php

namespace App\Auth;

use App\AdminUser; 
use Carbon\Carbon;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable; 
use Illuminate\Contracts\Auth\UserProvider;


class EloquentAdminUserProvider implements UserProvider {
	

	/**
	 * Retrieve a user by their unique identifier.
	 *
	 * @param  mixed $identifier
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveById($identifier)
	{
	
	   $qry = AdminUser::where('id','=',$identifier);
	    if($qry->count() >0)
	    {
	        $admin = $qry->select('id', 
	        	'first_name',
	        	'last_name', 
	        	'email',
	        	'password',
	        	'last_login_time'
	        )->first();

	      
	        return $admin;
	    }
	    return null;
	}

	/**
	 * Retrieve a user by by their unique identifier and "remember me" token.
	 *
	 * @param  mixed $identifier
	 * @param  string $token
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByToken($identifier, $token)
	{
		
	   
	    $qry = AdminUser::where('id','=',$identifier)->where('remember_token','=',$token);

	    if($qry->count() >0)
	    {
	       $admin = $qry->select('id', 'first_name','last_name', 'email','password')->first();
	        return $admin;
	    }
	    return null;



	}

	/**
	 * Update the "remember me" token for the given user in storage.
	 *
	 * @param  \Illuminate\Contracts\Auth\Authenticatable $user
	 * @param  string $token
	 * @return void
	 */
	public function updateRememberToken(Authenticatable $user, $token)
	{
		$user->setRememberToken($token);
	    $user->save();
	}

	/**
	 * Retrieve a user by the given credentials.
	 *
	 * @param  array $credentials
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByCredentials(array $credentials)
	{
		
	   
	    $qry = AdminUser::where('email','=',$credentials['email']);

	    if($qry->count() >0)
	    {
            $admin = $qry->select('id', 'first_name','last_name', 'email','password')->first();
            return $admin;
	    }
	
	    return null;
	}

	/**
	 * Validate a user against the given credentials.
	 *
	 * @param  \Illuminate\Contracts\Auth\Authenticatable $user
	 * @param  array $credentials
	 * @return bool
	 */
	public function validateCredentials(Authenticatable $user, array $credentials)
	{

	    // we'll assume if a user was retrieved, it's good
		if($user->email == $credentials['email'] && \Hash::check($credentials['password'], $user->getAuthPassword()))
	    {

	        $user->last_login_time = Carbon::now();
	        $user->save();

	        return true;
	    }
	    
	    return false;
	}


}