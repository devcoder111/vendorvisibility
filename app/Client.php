<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    const PROCESS_STATE = 1;
    const NOT_PROCESS_STATE = 0;

    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function scopeNotProcessed($query)
    {
    	return $query->where('is_processed',0);
    }

    public function scopeProcessed($query)
    {
    	
    	return $query->where('is_processed',1);
    }

    public static function createOrUpdate(array $clientArr)
    {
    	$existingRecordsArr = self::pluck('name','id')->toArray();

		# if no records
		if (empty($existingRecordsArr)) {
    		foreach ($clientArr as $client) {
    			self::create(['name'=>$client]);
    		}
    	}

    	# if existing records
    	if (!empty($existingRecordsArr)) {
    		$deleteClientArr = array_diff($existingRecordsArr, $clientArr);
    		if (!empty($deleteClientArr)) {
    			self::whereIn('name', $deleteClientArr)->delete();
    		}

    		foreach ($clientArr as $client) {
    			if (!in_array($client, $existingRecordsArr)) {
    				self::create(['name'=>$client]);
    			}
    		}
		}
	}

	public static function getListingByActiveClient($active_client)
	{
		if (!empty($active_client)) {
			return self::notProcessed()->orderByRaw("name = '".$active_client."' desc")->get();
		} else {
			return self::orderBy('name','desc')->get();
		}
	}

	public static function resetProcessState()
	{
		return self::update(['is_processed',0]);
	}

    public function setProcessedState($process_state)
    {
        $this->is_processed = $process_state;
        $this->save();
    }
}
