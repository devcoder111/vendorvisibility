<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\AmazonVendorAccountScrapService as VendorAccountScrapper;

class ScrapAmazonCentralAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amazon-central-account-scrapping:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To scrap amazon central account daily at 4 am.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scrapper = new VendorAccountScrapper;
        $scrapper->run();
        dd('Done');
    }
}
