<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Spark\Spark;
use Laravel\Spark\Http\Controllers\Auth\LoginController as SparkLoginController;

class LoginController extends SparkLoginController
{

    protected $redirectTo = '/home';
    
   public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }
    

    public function login(Request $request)
    {
        if ($request->has('remember')) {
            $request->session()->put('spark:auth-remember', $request->remember);
        }
        $user = Spark::user()->where(['email' => $request->email, 'status' => 1])->first();
        if (Spark::usesTwoFactorAuth() && $user && $user->uses_two_factor_auth) {
            $request->merge(['remember' => '']);
        }
        return $this->traitLogin($request);
    }

    public function authenticated(Request $request, $user)
    {
        if ($user->status != 1) {
            $this->guard()->logout();
            session()->flush();
            return redirect('/login')->with('successMessage', 'Your account is deactivated. Please contact the administrator.');
        }
        return parent::authenticated($request, $user);
    }

    public function logout()
    {
        $this->guard()->logout();

        session()->flush();

        return redirect('/login');
           
    }
}
