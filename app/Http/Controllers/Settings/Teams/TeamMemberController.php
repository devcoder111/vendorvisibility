<?php

namespace App\Http\Controllers\Settings\Teams;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamMemberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update the given team member.
     *
     * @param  Request  $request
     * @param  \Laravel\Spark\Team  $team
     * @param  mixed  $member
     * @return Response
     */
    public function update(Request $request, $team, $member)
    {
        $request->validate([
            'name' => 'required',
            'role' => 'required',
            'status' => 'required'
        ]);

        $member->update([
            'name' => request('name'),
            'status'=>request('status')
        ]);

        $team = $member->teams->find($team->id);
        $team->pivot->update([
            'role'=>request('role')
        ]);

        return response()->json($member);
    }

}
