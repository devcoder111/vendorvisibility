<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VendorInvoice;

class VendorCentralController extends Controller
{
    
    public function __construct()
    {
    }

    
    public function getAllInvoices()
    {

        $invoiceCollection = VendorInvoice::paginate(50);
        return view('invoice.index',compact('invoiceCollection'));
    }
}
