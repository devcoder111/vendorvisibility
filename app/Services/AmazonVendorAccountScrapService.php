<?php

namespace App\Services;

use Illuminate\Http\Request;

use Goutte\Client as GoutteClient;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Cookie\CookieJar;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\BrowserKit\Cookie;
use OTPHP\TOTP;
use Carbon\Carbon;

use App\VendorAccountSession;
use App\Client;
use App\VendorInvoice;


Class AmazonVendorAccountScrapService
{
	protected $client;
    protected $request;

	public function __construct()
	{
		$this->request = new Request;
        $this->client = new GoutteClient;
	}

	# For Testing purpose
    private function getOutput()
    {

        echo($this->client->getResponse()->getContent());#die;
    }

    private function logger($message)
    {
    	#\Log::info("Method Name : debug_backtrace()[1]['function']");
    	\Log::info(print_r($message,true));
    	return;
    }

    #####

    public function run()
    {
		$this->logger('Scrapper Starts.');

        $this->setHeaders(); 		

		$this->attemptLogin(); 	

        $this->goToHomepage();	

        $this->fetchClients();  	

        $this->getAllInvoices(); 	

        $this->logger("Scrapper Finished.");

        dd('Scrapper Finished.');

    }

    private function setHeaders()
    {
        $this->logger('Setting Headers.');

        $this->client->setHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
        $this->client->setHeader('Accept-Encoding', 'gzip, deflate, br');
        $this->client->setHeader('Accept-Language', 'en-US,en;q=0.9');
        $this->client->setHeader('Cache-Control', 'max-age=0');
        $this->client->setHeader('Connection', 'keep-alive');
        $this->client->setHeader('Upgrade-Insecure-Requests', '1');
        $this->client->setHeader('User-Agent', $this->request->header('User-Agent'));
        
        $this->logger('Headers has been set.');
    }

    private function attemptLogin()
    {
    	$this->logger('Login Attempt Starts');

    	$login_session = VendorAccountSession::where('type','login')->first();

    	empty($login_session)
            ? $this->setLoginSession()
            : $this->getLoginSession($login_session);
	}

    private function getLoginSession(VendorAccountSession $login_session)
    {
        $this->logger("Existing Login Session Found. Attempting Login with it.");

        $cookieCollection  = unserialize($login_session->session);
        $cookieJar = $this->client->getCookieJar();
        foreach ($cookieCollection as $key => $cookie) {
            $cookieJar->set($cookie);
        }
        $this->logger('Existing login set. Requesting Homepage by using it.');
	}

    private function setLoginSession()
    {
        $this->logger('Login Session not found. Setting new one.');
        
        # Sign In Link Page
        $url = 'https://vendorcentral.amazon.com/gp/vendor/sign-in';
        $this->logger("Requested URL : {$url}");
        $crawler = $this->client->request('GET', $url);

        $trackingPixelUrl = 'https://vendorcentral.amazon.com'.$crawler->filter('noscript img')->attr('src');
        $this->logger("Requested URL : {$trackingPixelUrl}");
        $this->client->request('GET', $trackingPixelUrl);

        $link = $crawler->selectLink('Sign in to Vendor Central')->link();
        $this->logger("Go to Sign in to Vendor Central Page");
        $crawler = $this->client->click($link);


        # Login Authentication Page
        $url = $this->client->getRequest()->getUri();
        $trackingPixels = $crawler->filter('img[style*="display:none"]')->each(function (Crawler $node, $i) {
            return $node->attr('src');
        });

        if (!empty($trackingPixels)) {
            foreach($trackingPixels as $p){
                $this->client->setHeader('Referer', $url);
                $this->logger("Requested URL : {$p}");
                $this->client->request('GET', 'https:'.$p);
            }
            $this->client->removeHeader('Referer');
        }

        # Get Login Authentication Form and submit with credentials
        $form = $crawler->selectButton('signInSubmit')->form();
        $this->client->setHeader('Referer',$url);
        $this->logger("About to submit Authentication Form");
        $crawler = $this->client->submit($form,['email'=>env('VENDOR_CENTRAL_ACCOUNT_EMAIL'),'password'=>env('VENDOR_CENTRAL_ACCOUNT_PASSWORD')]);
        $this->logger("Authentication Form submission complete");

        # For 2FA
        if(strpos(strtolower($crawler->filter('title')->text()),'two-step') !== false) 
        { 
            $this->logger("2FA encounter");
            $totp = new TOTP(env('VENDOR_CENTRAL_ACCOUNT_EMAIL'),env('VENDOR_CENTRAL_ACCOUNT_PASSWORD_SECRET_KEY'));
            $form = $crawler->selectButton('mfaSubmit')->form();
            $this->logger("2FA form about to submit.");
            $crawler = $this->client->submit($form,['otpCode'=>$totp->now()]);
            $this->logger("2FA form submission complete.");
        }

        $this->logger("Save session in DB");
        VendorAccountSession::create([
            'type' => 'login',
            'session' =>serialize($this->client->getCookieJar()->all())
        ]);

        $this->logger('New session set. Requesting homepage.');
    }

    private function goToHomepage()
    {
    	$url = 'https://vendorcentral.amazon.com/hz/vendor/members/home/ba';
        $this->client->request('GET', $url);

        ## Todo : 
        #) If existing session unable to reach homepage then we need to delete the existing session and set new one.
        #) For this, sleep must be implemented with logger.
        #) Need to do something for Cookie session timeout
    }

    private function selectAccountSwitch()
    {
    	$url = 'https://vendorcentral.amazon.com/hz/vendor/members/vendor-group-switcher/resource';
        $this->logger("Requested URL : {$url}");
        $this->client->request('GET', $url);
    }
     

    private function fetchClients()
    {
    	$this->logger('Fetching Client Details');

    	$this->selectAccountSwitch();

    	$clientArr = $this->client->getCrawler()->filter('option')->each(function (Crawler $node, $i) {
        	$str = $node->text();
        	$search = array("\n","(current account)", "US - ");
        	$replace = '';
            return trim(str_replace($search, $replace, $str));
        });

        Client::createOrUpdate($clientArr);

        $this->logger('Finished Fetching Client Details');
	}

    private function requestInvoices($start_date,$end_date,$startIndex)
    {
        $session_id = $this->client->getCookieJar()->get('session-id')->getValue();
        $url = 'https://vendorcentral.amazon.com/st/vendor/members/inv-mgmt/search/invoice-search-results-ajax?_='.time().'&startIndex='.$startIndex.'&endDate='.$end_date.'&useLegacySearch=false&searchKey=invoiceDateCriteria&invoiceStatus=&batchSize=200&startDate='.$start_date.'&sessionId='.$session_id;

        \Log::info("Start Date : {$start_date}, End Date : {$end_date}, startIndex : {$startIndex}");
        $this->client->request('GET', $url);

        $fetchedInvoicesObj = json_decode($this->client->getResponse()->getContent());
        
        return $fetchedInvoicesObj;
    }

    private function retryFetchingInvoices($start_date,$end_date,$startIndex)
    {
        $retry = 1;
        while ($retry >= 1 && $retry <= 5) {
            $this->logger("retry number : $retry");
            sleep($retry*3);
            $invoiceObj = $this->requestInvoices($start_date,$end_date,$startIndex);
            $empty_response = (empty($invoiceObj) || empty($invoiceObj->data)) ? (true) : (false);
            $this->logger("empty_response : $empty_response");
            $retry = $empty_response ? ($retry + 1) : (0);
        }
        return $invoiceObj;
    }

    private function fetchInvoices($start_date,$end_date,$startIndex)
    {
       $fetchedInvoicesObj = $this->requestInvoices($start_date,$end_date,$startIndex);

        if (empty($fetchedInvoicesObj) || empty($fetchedInvoicesObj->data)) {
            $fetchedInvoicesObj = $this->retryFetchingInvoices($start_date,$end_date,$startIndex);
        }

        return $fetchedInvoicesObj;
    }

    private function getActiveClientName()
    {
    	$this->logger('Fetching active client');
    	$this->goToHomepage();
        $active_client = trim($this->client->getCrawler()->filter('.greeting-cell')->text());
        return str_after($active_client,'Hello, ');
    }

    private function switchAccount()
    {
    	$this->logger('Switcing Account');
    	
    	$this->selectAccountSwitch();

        $associatedClientArr = $this->client->getCrawler()->filter('option')->each(function (Crawler $node, $i) {
            return array($node->attr('value') => trim(str_replace('(current account)','',$node->text())));
        });

        dd($associatedClientArr);
        

        $form = $this->client->getCrawler()->filter('body > form')->form();
        $this->client->submit($form,['vendorGroup'=>'3292830']);

      
        $this->getOutput();
    }

    private function getAllInvoices()
    {
    	$active_client = $this->getActiveClientName();

    	$clientCollection = Client::getListingByActiveClient($active_client);

		$invoiceDataArr = [];

    	if ($clientCollection->isNotEmpty()) {
    		foreach ($clientCollection as $index => $client) {
    			$invoiceDataArr = [];
    			$this->logger($client->name);
    			sleep(3*$index);
    			$this->getOutput();
    			
    			if ($index == 0) {
    				$invoiceDataArr = $this->invoiceHandler($client);
    			}

    			if ($index > 0) {
					$this->selectAccountSwitch();
					$active_client = $client->name;

					$clientListing = $this->client->getCrawler()->filter('option')->each(function (Crawler $node, $i) {
						$str = $node->text();
		        		$search = array("\n","(current account)", "US - ");
		        		$replace = '';
						return array($node->attr('value') => trim(str_replace($search, $replace, $str)));
 					});

					$vendor_grp_id = '';
 					foreach ($clientListing as $key => $clientArr) {
 						foreach ($clientArr as $key => $clt_name) {
 							if ($clt_name == $client->name) {
 								$vendor_grp_id = $key;
 							}
 						}
 					}

 					if ($vendor_grp_id == '') {
 						continue;
 					}

 					$form = $this->client->getCrawler()->filter('body > form')->form();
        			$this->client->submit($form,['vendorGroup'=>$vendor_grp_id]);
        			
        			$invoiceDataArr = $this->invoiceHandler($client);
				}
	    	}
    	}
    	
    	return $invoiceDataArr;
    }

    private function invoiceHandler(Client $client, $startIndex = 0, $start_date = '', $end_date = '', $dataArr = [])
    {
        $duration = "-1 months";
        $fetch_upto_year = "2018";

        $start_date = ($start_date == '')
                        ? (date("m/d/Y", strtotime($duration)))
                        : ($start_date);

        $end_date = ($end_date == '')
                        ? (date("m/d/Y"))
                        : ($end_date);

        $invoiceDataObj = $this->fetchInvoices($start_date,$end_date,$startIndex);
    
        $invoiceDataArr = (!empty($invoiceDataObj) && !empty($invoiceDataObj->data)) 
                            ? ($invoiceDataObj->data)
                            : ([]);
	
        $count = count($invoiceDataArr);
        $this->logger("Invoice Fetched : $count");

        $fetchedInvoiceArr = array_merge($invoiceDataArr, $dataArr);
        
        $total_count = count($fetchedInvoiceArr);
        $this->logger("Total Invoices Found : $total_count");

        if (!empty($invoiceDataObj) && !empty($invoiceDataObj->data)) {
            $startIndex = $invoiceDataObj->values->startIndex;
            $this->invoiceHandler($client,$startIndex,$start_date,$end_date,$fetchedInvoiceArr);
        } else {
        	VendorInvoice::saveInvoices($fetchedInvoiceArr,$client);
        	$client->setProcessedState(Client::PROCESS_STATE);
		}
    	
        return $fetchedInvoiceArr;


         #### Child Finish #### #### Parent Start ####
        /*
        if (!empty($invoiceDataObj)) {
            
            $end_date =  $invoiceDataObj->values->startDate;
            $start_date = date('m/d/Y',strtotime($duration,strtotime($invoiceDataObj->values->startDate)));
            
            if (date('Y', strtotime($start_date)) == $fetch_upto_year) {
                $invoiceDataObj = $this->invoiceHandler(0,$start_date,$end_date,$fetchedInvoiceArr);
            }


        }*/
    }

}