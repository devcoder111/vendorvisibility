<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Client;

class VendorInvoice extends Model
{
    use SoftDeletes;

	protected $dates = ['deleted_at'];
    protected $guarded = [];

    public static function saveInvoices(array $fetchedInvoiceArr, Client $client)
    {
    	if (!empty($fetchedInvoiceArr)) {
            foreach ($fetchedInvoiceArr as $key => $invoice) 
            {
            	$finalArr[] = [
                    "client_id"                     => $client->id
                    ,"payee_code"                   => !empty($invoice->payee_code) 
                    									? ($invoice->payee_code) 
                    									: (null)
                    ,"invoice_number"               => !empty($invoice->invoice_number)
                    									? ($invoice->invoice_number) 
                    									: (null)
					,'creation_date'                => !empty($invoice->creation_date) 
                    									? (date('Y-m-d',strtotime($invoice->creation_date))) 
                    									: (null)
                    ,'invoice_date'                 => !empty($invoice->invoice_date)
                    									? (date('Y-m-d',strtotime($invoice->invoice_date))) 
                    									: (null)
                    ,'due_date'                     => !empty($invoice->due_date)
                    									? (date('Y-m-d',strtotime($invoice->due_date))) 
                    									: (null)
                    ,'invoice_amount'               => !empty($invoice->invoice_amount)
                    									? ($invoice->invoice_amount) 
                    									: (0)
					,'invoice_amount_currencyCode'  => !empty($invoice->invoice_amount_currencyCode)
                    									? ($invoice->invoice_amount_currencyCode) 
														: (null)
                    ,'invoice_source'               => !empty($invoice->invoice_source)
                    									? ($invoice->invoice_source) 
                    									: (null)
                    ,'paid_amount'                  => !empty($invoice->paid_amount)
                    									? ($invoice->paid_amount) 
                    									: (0)
                    ,'paid_amount_currencyCode'     => !empty($invoice->paid_amount_currencyCode)
                    									? ($invoice->paid_amount_currencyCode) 
                    									: (null)
                    ,'invoice_id'                   => !empty($invoice->invoice_id) 
                    									? ($invoice->invoice_id) 
                    									: (null)
                    ,'has_deductions'               => !empty($invoice->has_deductions) 
                    									? ($invoice->has_deductions) 
                    									: (null)
                    ,'invoice_status'               => !empty($invoice->invoice_statu) 
                    									? ($invoice->invoice_statu) 
                    									: (null)
                    ,'externalId'                   => !empty($invoice->externalId) 
                    									? ($invoice->externalId) 
                    									: (null)
                    ,'invoice_detail_link_info'     => !empty($invoice->action_list_hidden) 
                    									? ($invoice->action_list_hidden) 
                    									: (null)
                    ,'created_at'					=>  date('Y-m-d')
                    ,'updated_at'					=>  date('Y-m-d')
                ];
        	}
	        self::insert($finalArr);
    	}
    }
}
