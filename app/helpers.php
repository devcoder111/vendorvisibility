<?php 

function asset_timed($path, $secure = null) {
    $file = public_path($path);
    if (file_exists($file)) {
        return asset($path, $secure) . '?' . filemtime($file);
    } else {
        asset($path, $secure);
        if(env('APP_ENV') == 'local'){
            throw new Exception("File not found at path: $path");
        }
        if(env('APP_ENV') == 'production'){
            redirect('/404.html');
        }
    }
}

function includeJSFile($path) 
{
    return '<script src="' . url($path) . '" type="text/javascript"></script>';
}


function includeCssFile($path){
    return '<link href="' . url($path) . '" rel="stylesheet" type="text/css"/>';
}
