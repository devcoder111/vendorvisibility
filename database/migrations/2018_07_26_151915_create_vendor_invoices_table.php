<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->string('payee_code',20)->nullable();
            $table->string('invoice_number',20)->nullable();
            $table->date('creation_date')->nullable();
            $table->date('invoice_date')->nullable();
            $table->date('due_date')->nullable();
            $table->decimal('invoice_amount',10,2)->nullable();
            $table->string('invoice_amount_currencyCode',10)->nullable();
            $table->string('invoice_source',50)->nullable();
            $table->decimal('paid_amount',10,2);
            $table->string('paid_amount_currencyCode',10)->nullable();
            $table->string('invoice_id',50)->nullable();
            $table->string('has_deductions',10)->nullable();
            $table->string('invoice_status',10)->nullable();
            $table->string('externalId',50)->nullable();
            $table->text('invoice_detail_link_info')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_invoices');
    }
}
