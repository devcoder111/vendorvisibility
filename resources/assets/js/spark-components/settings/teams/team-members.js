var base = require('settings/teams/team-members');

Vue.component('spark-team-members', {
    mixins: [base],

    data() {
        return {
            roles: [],

            updatingTeamMember: null,
            deletingTeamMember: null,

            updateTeamMemberForm: $.extend(true, new SparkForm({
                name: '',
                role: '',
                status : ''
            }), Spark.forms.updateTeamMember),

            deleteTeamMemberForm: new SparkForm({})
        }
    },

    methods : {
    	 /**
         * Edit the given team member.
         */
        editTeamMember(member) {
        	this.updatingTeamMember = member;
            this.updateTeamMemberForm.role = member.pivot.role;
            this.updateTeamMemberForm.name = member.name;
            this.updateTeamMemberForm.status = member.status;

            $('#modal-update-team-member').modal('show');
        },
    }
});
