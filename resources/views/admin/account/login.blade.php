@extends('layouts.admin.master')
@section('content')

<section class="login header p-fixed d-flex text-center bg-primary common-img-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 mt-5">
                <!-- Authentication card start -->
                <div class="login-card card-block auth-body ">
                    <form method="POST" action="{{ route('admin.account.login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                        <div class="auth-box">
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <h3 class="text-left txt-primary">Admin Sign In</h3>
                                </div>
                            </div>
                            <hr/>

                            <div class="input-group">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Your Email Address" autofocus>
                                <span class="md-line"></span>
                            </div>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                <span class="md-line"></span>
                            </div>
                            
                            <div class="row m-t-30">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Sign in</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
