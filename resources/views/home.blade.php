@extends('layouts.master')
@section('content')

<home :user="user" inline-template>
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-inner-content">
                <div class="main-body" >
                    <div class="page-wrapper">
                        <div class="page-body">
                            <div class="row">
                                <div class="col-lg-12 ">
                                    <div class="card p-5">
                                        <div class="card-block">
                                            <h1 align="center">Welcome to Dashboard</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>
@endsection
