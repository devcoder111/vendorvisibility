@extends('layouts.master')
@section('content')

<div class="pcoded">
	<div class="pcoded-overlay-box"></div>
	<div class="pcoded-container navbar-wrapper">
		<div class="pcoded-main-container" style="margin-top: 0px;">
			<div class="pcoded-wrapper">
				<div class="pcoded-content ml-0">
					<div class="pcoded-inner-content">
						<div class="main-body">
							<div class="page-wrapper">
								<div class="page-body">
									<div class="card">
										<div class="card-header"> <h5> Invoices</h5> </div>
										<div class="card-block table-border-style">
											<div class="table-responsive">
												<table class="table">
													<thead>
														<tr>
															<th> # </th>
															<th> Payee </th>
															<th> Invoice Number </th>
															<th> Invoice Creation Date </th>
															<th> Invoice Date </th>
															<th> Due Date </th>
															<th> Invoice Amount </th>
															<th> Source </th>
															<th> Invoice Status </th>
														</tr>
													</thead>
													<tbody>
														@if($invoiceCollection->isNotEmpty())
															@php $i = 1; @endphp
															@foreach($invoiceCollection as $index => $invoice)
																<tr>
																	<th scope="row"> 
																		{{ (($invoiceCollection->currentPage() - 1 ) * $invoiceCollection->perPage()) + $i++  }} 
																	</th>
																	<td> {{ $invoice->payee_code}} </td>
																	<td> {{ $invoice->invoice_number}} </td>
																	<td> {{ $invoice->creation_date}} </td>
																	<td> {{ $invoice->invoice_date}} </td>
																	<td> {{ $invoice->due_date}} </td>
																	<td class="text-right"> ${{ $invoice->invoice_amount}} </td>
																	<td> 
																		{{ 
																			($invoice->invoice_source == 'VendorCentralWeb')
																			? ('Vendor Central')
																			: ($invoice->invoice_source)
																		}} 
																	</td>
																	<td> {{ $invoice->invoice_status}} </td>
																</tr>
															@endforeach
														@else 
															<tr><td colspan="10"> No Record Found </td></tr>
														@endif
													</tbody>
												</table>
											</div>
										</div>
										@if ($invoiceCollection->lastPage() > 1)
											<div class="card-footer">
												<div class="col-xs-12 text-center pull-right"> 
													{{ $invoiceCollection->links() }}
												</div>
									        </div>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	@endsection