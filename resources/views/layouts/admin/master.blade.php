<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <title>@yield('title', config('app.name'))</title>

    @include('layouts.partial.style')

    @include('layouts.partial.top_scripts')
</head>

<body>
    <div id="spark-app" v-cloak>

        <div id="pcoded" class="pcoded">
            <div class="pcoded-overlay-box"></div>
            <div class="pcoded-container navbar-wrapper">
                
                @if(false)
                <!-- Navigation -->
                @if (Auth::check())
                    @include('spark::nav.user')
                @else
                    @include('spark::nav.guest')
                @endif
                @endif

                <!-- Main Content -->
                <main>
                    @yield('content')
                </main>

                <!-- Application Level Modals -->
                @if(false)
                @if (Auth::check())
                    @include('spark::modals.notifications')
                    @include('spark::modals.support')
                    @include('spark::modals.session-expired')
                @endif
                @endif
            </div>
        </div>
    </div>
    @include('layouts.partial.bottom_scripts')
</body>
</html>
