<ul class="nav-right">
    <li class="header-notification ">
        <a href="{{url('/invoices')}}">
            <i class=" m-r-5"></i> Invoices
        </a>
    </li>

    <!-- Image with dropdown -->
    <li class="user-profile header-notification">

        <a href="javascript:void(0);">
            <img :src="user.photo_url" class="rounded-circle">
            <!-- <img src="{{url('/images/user.png')}}"> -->
            <span class="">@{{user.name}}</span>
            <i class="ti-angle-down"></i>
        </a>
        
        <ul class="show-notification profile-notification">
            <!-- Impersonation (Correbt Design)-->
            @if (session('spark:impersonator'))
                <li class="dropdown-header"> {{__('Impersonation')}} </li>

                <!-- Stop Impersonating -->
                <li>
                    <a href="/spark/kiosk/users/stop-impersonating">
                        <i class="fa fa-fw text-left fa-btn fa-user-secret"></i> {{__('Back To My Account')}}
                    </a>    
                </li>
            @endif

            <!-- Developer (Correct Design) -->
            @if (Spark::developer(Auth::user()->email))
                @include('spark::nav.developer')
            @endif

            <!-- Subscription Reminders -->
            @include('spark::nav.subscriptions')

            <!-- Settings -->
           
            <li><strong> {{__('Settings')}} </strong></li>
            <li>
                <a class="" href="/settings">
                    <i class="ti-settings"></i> {{__('Your Settings')}}
                </a>
            </li>

            @if (Spark::usesTeams() && (Spark::createsAdditionalTeams() || Spark::showsTeamSwitcher()))
                <!-- Team Settings -->
                @include('spark::nav.teams')
            @endif

            @if (Spark::hasSupportAddress())
                <!-- Support -->
                @include('spark::nav.support')
            @endif

            <!-- Logout -->
            <li>
                <a class="" href="/logout">
                    <i class="ti-arrow-circle-left"></i> {{__('Logout')}}
                </a>
            </li>
        </ul>
    </li>
</ul>