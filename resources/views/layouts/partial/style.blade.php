<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

<!-- Spark -->
<link href="{{ mix(Spark::usesRightToLeftTheme() ? 'css/app-rtl.css' : 'css/app.css') }}" rel="stylesheet">

<?php 

# Theme
$stylesheetArr = [
	'theme/lib/bootstrap/css/bootstrap.min.css'
	,'theme/css/themify-icons.css'
	,'theme/css/icofont.css'
	,'theme/css/component.css'
	,'theme/lib/horizontal-timeline/css/style.css'
	,'theme/lib/amchart/css/amchart.css'
	,'theme/css/flag-icon.min.css'
	,'theme/css/style.css'
	,'theme/css/color-1.css' //id="color"
	,'theme/css/linearicons.css'
	,'theme/css/simple-line-icons.css'
	,'theme/css/ionicons.css'
	,'theme/css/jquery.mCustomScrollbar.css'
	,'css/custom.css'
];

foreach ($stylesheetArr as $key => $stylesheet) { ?>
	<link rel="stylesheet" href="{{ asset_timed($stylesheet) }}" >
<?php } ?>



@stack('styles')





