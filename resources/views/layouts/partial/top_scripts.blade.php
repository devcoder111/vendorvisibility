@yield('scripts', '')

<!-- Global Spark Object -->
<script>
    window.Spark = <?php echo json_encode(array_merge(
        Spark::scriptVariables(), []
    )); ?>;
</script>


<?php 

$scriptArr = [
	    'js/vendor/jquery/jquery.min.js'
    ,'js/vendor/jquery/jquery-ui.min.js'
    ,'theme/js/tether.min.js'
    ,'js/vendor/popper.min.js'
    ,'theme/lib/bootstrap/js/bootstrap.min.js'
    ,'theme/js/jquery.slimscroll.js'
    ,'theme/js/modernizr.js'
    ,'theme/js/css-scrollbars.js'
    ,'theme/js/classie.js'
    ,'theme/js/d3.min.js'
    ,'theme/js/rickshaw.js'
    ,'theme/js/raphael.min.js'
    ,'theme/js/morris.js'
    ,'theme/lib/horizontal-timeline/js/main.js'
    ,'theme/lib/amchart/js/amcharts.js'
    ,'theme/lib/amchart/js/serial.js'
    ,'theme/lib/amchart/js/light.js'
    ,'theme/lib/amchart/js/custom-amchart.js'
    ,'theme/js/i18next.min.js'
    ,'theme/js/i18nextXHRBackend.min.js'
    ,'theme/js/i18nextBrowserLanguageDetector.min.js'
    ,'theme/js/jquery-i18next.min.js'
    ,'theme/js/custom-dashboard.js'
    ,'theme/js/script.js'
    ,'theme/js/pcoded.min.js'
    ,'theme/js/demo-12.js'
    ,'theme/js/jquery.mCustomScrollbar.concat.min.js'
    ,'theme/js/jquery.mousewheel.min.js'

];


foreach ($scriptArr as $key => $script) { ?>
	<script type="text/javascript" src="{{ asset_timed($script) }}"></script>
<?php } ?>