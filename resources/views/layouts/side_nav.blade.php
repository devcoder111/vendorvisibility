<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <nav class="pcoded-navbar" pcoded-header-position="relative">
            <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
            <div class="pcoded-inner-navbar main-menu">
                <!-- <div class="">
                    <div class="main-menu-header">
                        <img class="img-40" src="assets/images/user.png" alt="User-Profile-Image">
                        <div class="user-details">
                            <span>John Doe</span>
                            <span id="more-details">UX Designer<i class="ti-angle-down"></i></span>
                        </div>
                    </div>

                    <div class="main-menu-content">
                        <ul>
                            <li class="more-details">
                                <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                                <a href="#!"><i class="ti-settings"></i>Settings</a>
                                <a href="#!"><i class="ti-layout-sidebar-left"></i>Logout</a>
                            </li>
                        </ul>
                    </div>
                </div> -->
                @yield('side_nav_content')
            </div>
        </nav>
    </div>
</div>