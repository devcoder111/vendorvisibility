<div class="navbar-logo">
    <a class="mobile-menu" id="mobile-collapse" href="#!">
        <i class="ti-menu"></i>
    </a>
    
    <!-- Todo : need to manage in middleware -->
    <a href="{{in_array(\URL::current(),[url('login'),url('register')])?url('/'): url('/home')}}">
        <img class=" h-37 w-auto" src="{{url('/img/logo.png')}}" alt="Theme-Logo" />
    </a>
    
    <a class="mobile-options">
        <i class="ti-more"></i>
    </a>
</div>