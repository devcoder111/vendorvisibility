<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <nav class="navbar header-navbar pcoded-header p-0" header-theme="theme4">
            <div class="navbar-wrapper">

                @if(false)
                    @include('spark::nav.brand')
                @endif
                
                <div class="navbar-container container-fluid">
                    <div>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <a href="/login" class="{{url('login') != \URL::current() ? ('') : ('text-warning')}}">
                                    <i class="m-r-5"></i> Login 
                                </a>

                                <a href="/register" class="{{url('register') != \URL::current() ? ('') : ('text-warning')}}">
                                    <i class="m-r-5"></i> Register 
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>