<!-- Teams -->
<li><strong> {{__('teams.teams')}} </strong></li>
@if (Spark::createsAdditionalTeams())
<!-- Create Team -->
<li>
    <a class="" href="/settings#/{{Spark::teamsPrefix()}}">
        <i class="ti-plus"></i> {{__('teams.create_team')}}
    </a>
</li>
@endif

<!-- Switch Current Team -->
@if (Spark::showsTeamSwitcher())

    <li>
        <a v-for="team in teams" :href="'/settings/{{ Spark::teamsPrefix() }}/'+ team.id +'/switch'">
            <span v-if="user.current_team_id == team.id">
                <i class="ti-check"></i> @{{ team.name }}
            </span>

            <span v-else>
                <img :src="team.photo_url" class="spark-profile-photo-xs"><i class="fa fa-btn"></i> @{{ team.name }}
            </span>
        </a>
    </li>
@endif
