<!-- NavBar For Authenticated Users -->
<spark-navbar
:user="user"
:teams="teams"
:current-team="currentTeam"
:unread-announcements-count="unreadAnnouncementsCount"
:unread-notifications-count="unreadNotificationsCount"
inline-template>

    <nav class="navbar header-navbar pcoded-header p-0" header-theme="theme4">
        <div class="navbar-wrapper" v-if="user">
            
            @if(false)
                @include('spark::nav.brand')
            @endif
                
            <div class="navbar-container container-fluid" v-if="user">
                <div>

                    @includeIf('spark::nav.user-left') 

                    @include('layouts.partial.nav_right')
            
                </div>
            </div>
        </div>
    </nav>
</spark-navbar>
