@extends('spark::layouts.app')

@section('scripts')
    @if (Spark::billsUsingStripe())
        <script src="https://js.stripe.com/v3/"></script>
    @else
        <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
    @endif
@endsection

@section('content')
    <spark-settings :user="user" :teams="teams" inline-template>
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <nav class="pcoded-navbar" pcoded-header-position="relative">
                    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                    <div class="pcoded-inner-navbar main-menu">
                         <ul class="pcoded-item pcoded-left-item">
                        <li class="pcoded-hasmenu active  pcoded-trigger">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="ti-settings"></i></span>
                                <span class="pcoded-mtext" data-i18n="nav.dash.main">{{__('Settings')}}</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">
                                <li class="">
                                    <a class="nav-link" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Profile')}}</span>
                                    </a>
                                </li>

                                 @if (Spark::usesTeams())
                                <li class="">
                                    <a class="nav-link" href="#{{Spark::teamsPrefix()}}" aria-controls="teams" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('teams.teams')}}</span>
                                    </a>
                                </li>
                                
                                <li class="">
                                    <a class="nav-link" href="#security" aria-controls="security" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Security')}}</span>
                                    </a>
                                </li>

                                @if (Spark::usesApi())
                                    <li class="">
                                        <a class="nav-link" href="#security" aria-controls="security" role="tab" data-toggle="tab">
                                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('API')}}</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        
                        @if (Spark::canBillCustomers())
                        <li class="pcoded-hasmenu">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="ti-credit-card"></i></span>
                                <span class="pcoded-mtext" data-i18n="nav.dash.main"> {{__('Billing')}}</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">
                                @if (Spark::hasPaidPlans())
                                <li class="">
                                    <a class="nav-link" href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default"> {{__('Subscription')}}</span>
                                    </a>
                                </li>
                                @endif
                                
                                <li class="">
                                    <a class="nav-link" href="#payment-method" aria-controls="payment-method" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Payment Method')}}</span>
                                    </a>
                                </li>

                                <li class="">
                                    <a class="nav-link" href="#invoices" aria-controls="invoices" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Invoices')}}</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li> 
                        @endif   
                    </ul>
                    </div>
                </nav>

                <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- Main-body start -->
                    <div class="main-body" >
                        <div class="page-wrapper">
                            <!-- Page header start -->
                          <!--   <div class="page-header">
                                <div class="page-header-title">
                                    <h4>CRM Dashboard</h4>
                                </div>
                                
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="index.html">
                                                <i class="icofont icofont-home"></i>
                                            </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
                                        
                                        <li class="breadcrumb-item"><a href="#!">CRM</a></li>
                                    </ul>
                                </div>
                            </div> -->
                            <!-- Page header end -->

                            <!-- Page body start -->
                            <div class="page-body">
                                
                                <div class="row">
                                    <div class="col-lg-12 ">
                                        <!-- Profile -->
                                        <div role="tabcard" class="tab-pane active" id="profile">
                                            @include('spark::settings.profile')
                                        </div>

                                        <!-- Teams -->
                                        @if (Spark::usesTeams())
                                            <div role="tabcard" class="tab-pane" id="{{Spark::teamsPrefix()}}">
                                                @include('spark::settings.teams')
                                            </div>
                                        @endif

                                         <!-- Security -->
                    <div role="tabcard" class="tab-pane" id="security">
                        @include('spark::settings.security')
                    </div>

                    <!-- API -->
                    @if (Spark::usesApi())
                    <div role="tabcard" class="tab-pane" id="api">
                        @include('spark::settings.api')
                    </div>
                    @endif

                    <!-- Billing Tab Panes -->
                    @if (Spark::canBillCustomers())
                    @if (Spark::hasPaidPlans())
                    <!-- Subscription -->
                    <div role="tabcard" class="tab-pane" id="subscription">
                        <div v-if="user">
                            @include('spark::settings.subscription')
                        </div>
                    </div>
                    @endif

                    <!-- Payment Method -->
                    <div role="tabcard" class="tab-pane" id="payment-method">
                        <div v-if="user">
                            @include('spark::settings.payment-method')
                        </div>
                    </div>

                    <!-- Invoices -->
                    <div role="tabcard" class="tab-pane" id="invoices">
                        @include('spark::settings.invoices')
                    </div>
                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </spark-settings>
@endsection
