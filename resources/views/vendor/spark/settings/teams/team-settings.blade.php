@extends('spark::layouts.app')

@section('scripts')
@if (Spark::billsUsingStripe())
<script src="https://js.stripe.com/v3/"></script>
@else
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
@endif
@endsection

@section('content')
<spark-team-settings :user="user" :team-id="{{ $team->id }}" inline-template>

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <nav class="pcoded-navbar" pcoded-header-position="relative">
                <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                <div class="pcoded-inner-navbar main-menu">
                    <!-- <div class="">
                        <div class="main-menu-header">
                            <img class="img-40" src="assets/images/user.png" alt="User-Profile-Image">
                            <div class="user-details">
                                <span>John Doe</span>
                                <span id="more-details">UX Designer<i class="ti-angle-down"></i></span>
                            </div>
                        </div>

                        <div class="main-menu-content">
                            <ul>
                                <li class="more-details">
                                    <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                                    <a href="#!"><i class="ti-settings"></i>Settings</a>
                                    <a href="#!"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div> -->

                    <ul class="pcoded-item pcoded-left-item">
                        <li class="pcoded-hasmenu active  pcoded-trigger">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="ti-settings"></i></span>
                                <span class="pcoded-mtext" data-i18n="nav.dash.main">{{__('teams.team_settings')}}</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">
                                @if (Auth::user()->ownsTeam($team))
                                <li class="">
                                    <a class="nav-link" href="#owner" aria-controls="owner" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('teams.team_profile')}}</span>
                                    </a>
                                </li>
                                @endif

                                <li class="">
                                    <a class="nav-link" href="#membership" aria-controls="membership" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Membership')}}</span>
                                    </a>
                                </li>

                                @if (Spark::createsAdditionalTeams())
                                <li class="">
                                    <a class="nav-link" href="/settings#/{{str_plural(Spark::teamsPrefix())}}">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('teams.view_all_teams')}}</span>
                                    </a>
                                </li>
                                @else
                                <li class="">
                                    <a class="nav-link" href="/settings">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Your Settings')}}</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        
                        @if (Spark::canBillTeams() && Auth::user()->ownsTeam($team))
                        <li class="pcoded-hasmenu">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="ti-credit-card"></i></span>
                                <span class="pcoded-mtext" data-i18n="nav.dash.main"> {{__('teams.team_billing')}}</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">
                                @if (Spark::hasPaidTeamPlans())
                                <li class="">
                                    <a class="nav-link" href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Subscription')}}</span>
                                    </a>
                                </li>
                                
                                <li class="">
                                    <a class="nav-link" href="#payment-method" aria-controls="payment-method" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Payment Method')}}</span>
                                    </a>
                                </li>

                                <li class="">
                                    <a class="nav-link" href="#invoices" aria-controls="invoices" role="tab" data-toggle="tab">
                                        <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.default">{{__('Invoices')}}</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li> 
                        @endif   
                    </ul>
                </div>
            </nav>

            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- Main-body start -->
                    <div class="main-body" >
                        <div class="page-wrapper">
                            <!-- Page header start -->
                          <!--   <div class="page-header">
                                <div class="page-header-title">
                                    <h4>CRM Dashboard</h4>
                                </div>
                                
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="index.html">
                                                <i class="icofont icofont-home"></i>
                                            </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
                                        
                                        <li class="breadcrumb-item"><a href="#!">CRM</a></li>
                                    </ul>
                                </div>
                            </div> -->
                            <!-- Page header end -->

                            <!-- Page body start -->
                            <div class="page-body">
                                
                                <div class="row">
                                    <div class="col-lg-12 ">

                                        <!-- <div class="card"> -->
                                           <!-- Owner Information -->
                                            @if (Auth::user()->ownsTeam($team))
                                                <div role="tabcard" class="tab-pane active" id="owner">
                                                    @include('spark::settings.teams.team-profile')
                                                </div>
                                            @endif

                                            <!-- Membership -->
                                            @if (Auth::user()->ownsTeam($team))
                                                <div role="tabcard" class="tab-pane" id="membership">
                                            @else
                                                <div role="tabcard" class="tab-pane active" id="membership">
                                            @endif
                                                    <div v-if="team">
                                                         @include('spark::settings.teams.team-membership')
                                                    </div>
                                                </div>

                                            <!-- Billing Tab Panes -->
                                            @if (Spark::canBillTeams() && Auth::user()->ownsTeam($team))
                                                @if (Spark::hasPaidTeamPlans())
                                                    <!-- Subscription -->
                                                    <div role="tabcard" class="tab-pane" id="subscription">
                                                        <div v-if="user && team">
                                                            @include('spark::settings.subscription')
                                                        </div>
                                                    </div>
                                                @endif

                                                <!-- Payment Method -->
                                                <div role="tabcard" class="tab-pane" id="payment-method">
                                                    <div v-if="user && team">
                                                        @include('spark::settings.payment-method')
                                                    </div>
                                                </div>

                                                <!-- Invoices -->
                                                <div role="tabcard" class="tab-pane" id="invoices">
                                                    <div v-if="user && team">
                                                        @include('spark::settings.invoices')
                                                    </div>
                                                </div>
                                            @endif
                                        <!-- </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</spark-team-settings>
@endsection