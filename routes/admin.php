<?php 

Route::middleware('guest')->group(function () {
    Route::get('account/login', 'AccountController@showLoginForm')->name('account.login');
    Route::post('account/login', 'AccountController@login')->name('account.login');
});

Route::get('account/logout', 'AccountController@logout')->name('account.logout');

Route::get('/home',function(){
	dd('Welcome to admin dashboard');
});