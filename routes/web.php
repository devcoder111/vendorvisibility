<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('/login', 'Auth\LoginController@login');

Route::get('/logout','Auth\LoginController@logout');

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'HomeController@show');





Route::group(['middleware' => 'web'],function(){
	Route::get('/invoices','VendorCentralController@getAllInvoices')->middleware('auth');
	Route::put('/settings/teams/{team}/members/{team_member}', 'Settings\Teams\TeamMemberController@update');
});


Route::get('/vendor-central', 'VendorCentralController@index');
Route::get('/read-csv','VendorCentralController@readCSV');

